var id             = 0;
var total          = 0;
var products       = [];
var purchasePrices = [];
var sellingPrices  = [];
var quantities     = [];

$(document).ready(function() {

  $("#addProductModalBtn").unbind('click').bind('click', function() {
    $("#submitProductForm")[0].reset();
    $(".text-danger").remove();
    $(".form-group").removeClass('has-error').removeClass('has-success');

    $("#productCode").change(function(){
      var productCode = $("#productCode").val();
      if(productCode) {

          $(".text-danger").remove();
      		$(".form-group").removeClass('has-error').removeClass('has-success');
      		$('.div-loading').removeClass('div-hide');
      		$('.div-result').addClass('div-hide');

          $.ajax({
            url: 'php_action/fetchBillProduct.php',
      			type: 'post',
      			data: {productCode: productCode},
      			dataType: 'json',
            success:function(response) {

              $('.div-loading').addClass('div-hide');
      				$('.div-result').removeClass('div-hide');

              if(response.success == true){
                $("#productCode").val(response.product_code);
                $("#productName").val(response.product_id);
                $("#purchasePrice").val(response.purchase_price);
                $("#sellingPrice").val(response.selling_price);
                $("#stocks").val(response.stock);
                $("#productId").val(response.product_name);
              }
              else{
                $("#productCode").val("");
                $("#productName").val("");
                $("#purchasePrice").val("");
                $("#sellingPrice").val("");
                $("#stocks").val("");
                $("#quantity").val("");
                $("#productId").val("");
              }
            }
          });
      }
    });

    $("#productName").change(function(){
      var productId = $("#productName").val();

      if(productId) {
        $(".text-danger").remove();
        $(".form-group").removeClass('has-error').removeClass('has-success');
        $('.div-loading').removeClass('div-hide');
        $('.div-result').addClass('div-hide');
        $.ajax({
          url: 'php_action/fetchPurchaseProduct.php',
          type: 'post',
          data: {productId: productId},
          dataType: 'json',
          success:function(response) {
            $('.div-loading').addClass('div-hide');
            $('.div-result').removeClass('div-hide');

            $("#productCode").val(response.product_code);
            $("#productName").val(response.product_id);
            $("#purchasePrice").val(response.purchase_price);
            $("#sellingPrice").val(response.selling_price);
            $("#stocks").val(response.stock);
            $("#productId").val(response.product_name);
          }
        });
      }
      else{
        $("#productCode").val("");
        $("#purchasePrice").val("");
        $("#sellingPrice").val("");
        $("#stocks").val("");
        $("#quantity").val("");
        $("#productId").val("");
      }
    });

    $("#productCode").keypress(function(event){
      if (event.which == 13) {
        $("#productName").focus();
        return false;
      }
    });

    $("#submitProductForm").unbind('submit').bind('submit', function() {
      $(".text-danger").remove();
      $(".form-group").removeClass('has-error').removeClass('has-success');

      var productCode = $("#productCode").val();
			var productName = $("#productName").val();
			var purchasePrice = $("#purchasePrice").val();
			var sellingPrice = $("#sellingPrice").val();
      var currentStock = $("#stocks").val();
      var quantity = $("#quantity").val();
      var productId = $("#productId").val();
      var success = true;

      if(productCode == "") {
				$("#productCode").after('<p class="text-danger">Product Code field is required</p>');
				$('#productCode').closest('.form-group').addClass('has-error');
        success = false;
			}	else {
				$("#productCode").find('.text-danger').remove();
				$("#productCode").closest('.form-group').addClass('has-success');
			}

      if(productName == "") {
				$("#productName").after('<p class="text-danger">Product Name field is required </p>' );
				$('#productName').closest('.form-group').addClass('has-error');
        success = false;
			}	else {
				$("#productName").find('.text-danger').remove();
				$("#productName").closest('.form-group').addClass('has-success');
			}

      if(/^[0-9]+\.?[0-9]*$/.test(purchasePrice) ) {
        $("#purchasePrice").find('.text-danger').remove();
				$("#purchasePrice").closest('.form-group').addClass('has-success');
			}	else {
        $("#purchasePrice").after('<p class="text-danger">Invalid Purchase Price </p>');
				$('#purchasePrice').closest('.form-group').addClass('has-error');
        success = false;
			}

      if(/^[0-9]+\.?[0-9]*$/.test(sellingPrice) ) {
        $("#sellingPrice").find('.text-danger').remove();
				$("#sellingPrice").closest('.form-group').addClass('has-success');
			}	else {
        $("#sellingPrice").after('<p class="text-danger">Invalid Selling Price </p>');
				$('#sellingPrice').closest('.form-group').addClass('has-error');
        success = false;
      }

      if(/^[0-9]+\.?[0-9]*$/.test(quantity) ) {
        $("#quantity").find('.text-danger').remove();
				$("#quantity").closest('.form-group').addClass('has-success');
			}	else {
        $("#quantity").after('<p class="text-danger"> Invalid Quantity</p>');
				$('#quantity').closest('.form-group').addClass('has-error');
        success = false;
      }

      if (success) {
        id++;

        purchasePrice = parseFloat(purchasePrice);
        sellingPrice = parseFloat(sellingPrice);
        quantity = parseFloat(quantity);

        var tot = purchasePrice*quantity;
        var tota = parseFloat($("#totalAmount").val()) + tot;
        var grand = tota-parseFloat($("#discount").val());

        purchasePrice = purchasePrice.toFixed(2);
        sellingPrice = sellingPrice.toFixed(2);

        quan = quantity.toString();
        if(quan.indexOf('.') === -1){
          quantity = quantity.toString();
        }
        else{
          quantity = quantity.toFixed(3);
        }


        tot = tot.toFixed(2);
        tota = tota.toFixed(2);
        grand = grand.toFixed(2);

        products.push(productName);
        purchasePrices.push(purchasePrice);
        sellingPrices.push(sellingPrice);
        quantities.push(quantity);

        $('<tr><td>' + id + '</td><td>'+ productId +'</td><td>'+ purchasePrice +'</td><td>'+ quantity +'</td><td>'+ tot + '</td><td><button class="btn btn-default removeProductRowBtn" type="button" id="removeProductRowBtn" onclick="removeProduct(this)"><i class="glyphicon glyphicon-trash"></i></button></td></tr> remove').appendTo($('#manageProductTable'));
        $("#totalAmount").val(tota);
        $("#grandTotal").val(grand);
        $("#productCode").focus();
        $("#submitProductForm")[0].reset();
      }
      return false;
    });
  });

  $("#discount").change(function(){
    var discount = $("#discount").val();
    if(/^[0-9]+\.?[0-9]*$/.test(discount) ) {
      discount = parseFloat(discount);
    }	else {
      discount = 0;
    }
    $("#discount").val(discount.toFixed(2));
    var grand = parseFloat($("#totalAmount").val())-discount;
    $("#grandTotal").val(grand.toFixed(2));
  });

})

function removeProduct(x = null){
  var $table = document.getElementById("manageProductTable");
  num = x.closest('tr').rowIndex;
  $table.deleteRow(num);
  num--;

  var purchasePrice = parseFloat(purchasePrices[num]);
  var quantity = parseFloat(quantities[num]);

  var tot = purchasePrice*quantity;
  var tota = parseFloat($("#totalAmount").val()) - tot;
  var grand = tota-parseFloat($("#discount").val());

  tot = tot.toFixed(2);
  grand = grand.toFixed(2);

  products.splice(num, 1);
  purchasePrices.splice(num, 1);
  sellingPrices.splice(num, 1);
  quantities.splice(num, 1);

  $("#totalAmount").val(tota);
  $("#grandTotal").val(grand);
  $('#manageProductTable > tbody > tr').each(function(i, val){
        $('td:first', this).text(i+1);
  });
  id--;``
}

function insert() {
  if (id == 0) {
    $("html, body, div.modal, div.modal-content, div.modal-body").animate({
      scrollTop: '0'
    }, 100);

    $(".success-messages").html('<div class="alert alert-warning">' +
      '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
      '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> Bill is Empty! </div>');

    $(".success-messages").delay(500).show(10, function() {
      $(this).delay(3000).hide(10, function() {
        $(this).hide();
      });
    });
  }
  else{
    $("#createOrderBtn").button('loading');
    var billNo = $("#billNo").val();
    var vendorName = $("#vendorName").val();
    var vendorContact = $("#vendorContact").val();
    var totalAmount = $("#totalAmount").val();
    var discount = $("#discount").val();
    var grandTotal = $("#grandTotal").val();
    $.ajax({
      url: 'php_action/insertPurchaseBill.php',
      type: 'post',
      data: {
        billNo: billNo,
        billDate: $("#orderDate").val(),
        vendorName: vendorName,
        vendorContact: vendorContact,
        totalAmount: totalAmount,
        discount: discount,
        grandTotal: grandTotal,
        products: products,
        purchasePrices: purchasePrices,
        sellingPrices: sellingPrices,
        quantities: quantities,
      },
      dataType: 'json',
      success: function(response) {
      if (response.success == true) {
        $("html, body, div.modal, div.modal-content, div.modal-body").animate({
          scrollTop: '0'
        }, 100);

        $(".success-messages").html('<div class="alert alert-success">' +
          '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
          '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> Bill Inserted! </div>');

        setTimeout(function() {
        location.reload();
      }, 1000);
    }
  }
    });
  }
  return false;
}
