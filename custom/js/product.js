var manageProductTable;

$(document).ready(function() {

  $('#navProduct').addClass('active');

  manageProductTable = $('#manageProductTable').DataTable({
		'ajax': 'php_action/fetchProduct.php',
		'order': []
	});

  $("#addProductModalBtn").unbind('click').bind('click', function() {

    $("#submitProductForm")[0].reset();
    $(".text-danger").remove();
    $(".form-group").removeClass('has-error').removeClass('has-success');

    $("#submitProductForm").unbind('submit').bind('submit', function() {

      $(".text-danger").remove();
      $(".form-group").removeClass('has-error').removeClass('has-success');

      var productCode = $("#productCode").val();
			var productName = $("#productName").val();
			var purchasePrice = $("#purchasePrice").val();
			var sellingPrice = $("#sellingPrice").val();
      var success = true;

      if(productCode == "") {
				$("#productCode").after('<p class="text-danger">Product Code field is required</p>');
				$('#productCode').closest('.form-group').addClass('has-error');
        success = false;
      }	else {
				$("#productCode").find('.text-danger').remove();
				$("#productCode").closest('.form-group').addClass('has-success');
			}

      if(productName == "") {
				$("#productName").after('<p class="text-danger">Product Name field is required</p>');
				$('#productName').closest('.form-group').addClass('has-error');
        success = false;
      }	else {
				$("#productName").find('.text-danger').remove();
				$("#productName").closest('.form-group').addClass('has-success');
			}

      if(/^[0-9]+\.?[0-9]*$/.test(purchasePrice) ) {
        $("#purchasePrice").find('.text-danger').remove();
				$("#purchasePrice").closest('.form-group').addClass('has-success');
      }	else {
        $("#purchasePrice").after('<p class="text-danger">Invalid Purchase Price </p>');
				$('#purchasePrice').closest('.form-group').addClass('has-error');
        success = false;
			}

      if(/^[0-9]+\.?[0-9]*$/.test(sellingPrice) ) {
        $("#sellingPrice").find('.text-danger').remove();
				$("#sellingPrice").closest('.form-group').addClass('has-success');
			}	else {
        $("#sellingPrice").after('<p class="text-danger">Invalid Selling Price </p>');
				$('#sellingPrice').closest('.form-group').addClass('has-error');
        success = false;
			}

      if(success) {

        $("#createProductBtn").button('loading');

        var form = $(this);
				var formData = new FormData(this);

        $.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							$("#createProductBtn").button('reset');

							$("#submitProductForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);

							$('#add-product-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +'</div>');

							$(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							});

		          manageProductTable.ajax.reload(null, true);

							$(".text-danger").remove();
							$(".form-group").removeClass('has-error').removeClass('has-success');

						}
					}
				});
      }
      return false;
    });
  });
});

function editProduct(productId = null) {
  if(productId) {

    $("#productId").remove();
		$(".text-danger").remove();
		$(".form-group").removeClass('has-error').removeClass('has-success');
		$('.div-loading').removeClass('div-hide');
		$('.div-result').addClass('div-hide');

    $.ajax({
      url: 'php_action/fetchSelectedProduct.php',
			type: 'post',
			data: {productId: productId},
			dataType: 'json',
      success:function(response) {

        $('.div-loading').addClass('div-hide');
				$('.div-result').removeClass('div-hide');

        $(".editProductFooter").append('<input type="hidden" name="productId" id="productId" value="'+response.product_id+'" />');

        $("#editProductCode").val(response.product_code);
        $("#editProductName").val(response.product_name);
        $("#editPurchasePrice").val(response.purchase_price);
        $("#editSellingPrice").val(response.selling_price);

        $("#editProductForm").unbind('submit').bind('submit', function() {
          $(".text-danger").remove();
          $(".form-group").removeClass('has-error').removeClass('has-success');

          var productCode = $("#editProductCode").val();
					var productName = $("#editProductName").val();
					var purchasePrice = $("#editPurchasePrice").val();
					var sellingPrice = $("#editSellingPrice").val();

          if(productCode == "") {
    				$("#editProductCode").after('<p class="text-danger">Product Code field is required</p>');
    				$('#editProductCode').closest('.form-group').addClass('has-error');
    			}	else {
    				$("#editProductCode").find('.text-danger').remove();
    				$("#editProductCode").closest('.form-group').addClass('has-success');
    			}

          if(productName == "") {
    				$("#editProductName").after('<p class="text-danger">Product Name field is required</p>');
    				$('#editProductName').closest('.form-group').addClass('has-error');
    			}	else {
    				$("#editProductName").find('.text-danger').remove();
    				$("#editProductName").closest('.form-group').addClass('has-success');
    			}

          if(/^[0-9]+\.?[0-9]*$/.test(purchasePrice) ) {
            $("#purchasePrice").find('.text-danger').remove();
    				$("#purchasePrice").closest('.form-group').addClass('has-success');
          }	else {
            $("#purchasePrice").after('<p class="text-danger">Invalid Purchase Price </p>');
    				$('#purchasePrice').closest('.form-group').addClass('has-error');
            success = false;
    			}

          if(/^[0-9]+\.?[0-9]*$/.test(sellingPrice) ) {
            $("#sellingPrice").find('.text-danger').remove();
    				$("#sellingPrice").closest('.form-group').addClass('has-success');
    			}	else {
            $("#sellingPrice").after('<p class="text-danger">Invalid Selling Price </p>');
    				$('#sellingPrice').closest('.form-group').addClass('has-error');
            success = false;
    			}

          if(productCode && productName && purchasePrice && sellingPrice) {
            	$("#editProductBtn").button('loading');

              var form = $(this);
						  var formData = new FormData(this);

              $.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								console.log(response);
								if(response.success == true) {

                  $("#editProductBtn").button('reset');

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);

									$('#edit-product-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									});

				          manageProductTable.ajax.reload(null, true);

									$(".text-danger").remove();
									$(".form-group").removeClass('has-error').removeClass('has-success');

								}
                else {

                  $("#editProductBtn").button('reset');

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);

									$('#edit-product-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									});

				          manageProductTable.ajax.reload(null, true);

									$(".text-danger").remove();
									$(".form-group").removeClass('has-error').removeClass('has-success');

								}

							}
						});
          }

          return false;
        });
      }
    });

  }
}

function removeProduct(productId = null) {
	if(productId) {

    $("#removeProductBtn").unbind('click').bind('click', function() {

      $("#removeProductBtn").button('loading');
			$.ajax({
				url: 'php_action/removeProduct.php',
				type: 'post',
				data: {productId: productId},
				dataType: 'json',
				success:function(response) {

          $("#removeProductBtn").button('reset');
					if(response.success == true) {

            $("#removeProductModal").modal('hide');

						manageProductTable.ajax.reload(null, false);

						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						});
					} else {

						$(".removeProductMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						});

					}
				}
			});
			return false;
		});
	}
}
