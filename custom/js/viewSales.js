var id             = 0;
var total          = 0;
var products       = [];
var purchasePrices = [];
var sellingPrices  = [];
var quantities     = [];

$(document).ready(function() {
  var billNo = $("#billNo").val();

  $.ajax({
    url: 'php_action/fetchSelectedSale.php',
    type: 'post',
    data: {billNo: billNo},
    dataType: 'json',
    success:function(response) {
      $("#orderDate").val(response.date);
      $("#vendorName").val(response.customer_name);
      $("#vendorContact").val(response.customer_contact);
      $("#totalAmount").val(response.total_amount);
      $("#discount").val(response.discount);
      $("#grandTotal").val(response.grand_total);
      var i;
      for (i = 0; i < response.products.length; i++) {
        id++;

        products.push(response.products[i]);

        sellingPrices.push(response.rate[i]);
        quantities.push(response.quantity[i]);
		productNames.push(response.products[i]);

        $('<tr><td>' + (i+1) + '</td><td>'+ response.products[i] +'</td><td>'+ response.rate[i] +'</td><td>'+ response.quantity[i] +'</td><td>'+ response.total[i] + '</tr> remove').appendTo($('#manageProductTable'));
      }
    }
  });
});

function insert() {

  if (id == 0) {
    $("html, body, div.modal, div.modal-content, div.modal-body").animate({
      scrollTop: '0'
    }, 100);

    $(".success-messages").html('<div class="alert alert-warning">' +
      '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
      '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> Bill is Empty! </div>');

    $(".success-messages").delay(500).show(10, function() {
      $(this).delay(3000).hide(10, function() {
        $(this).hide();
      });
    });
  }
  else{
    $("#createOrderBtn").button('loading');
    var billNo = $("#billNo").val();
    var billDate = $("#orderDate").val();
    var vendorName = $("#vendorName").val();
    var vendorContact = $("#vendorContact").val();
    var totalAmount = $("#totalAmount").val();
    var discount = $("#discount").val();
    var grandTotal = $("#grandTotal").val();

  // $.ajax({
  //     url: 'php_action/insertSalesBill.php',
  //     type: 'post',
  //     data: {
  //       billNo: billNo,
  //       billDate: billDate,
  //       vendorName: vendorName,
  //       vendorContact: vendorContact,
  //       totalAmount: totalAmount,
  //       discount: discount,
  //       grandTotal: grandTotal,
  //       products: products,
  //       sellingPrices: sellingPrices,
  //       quantities: quantities,
  //     },
  //     dataType: 'json',
  //     success: function(response) {
  //     if (response.success == true) {
  //       $("html, body, div.modal, div.modal-content, div.modal-body").animate({
  //         scrollTop: '0'
  //       }, 100);
  //
  //       $(".success-messages").html('<div class="alert alert-success">' +
  //         '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
  //         '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> Bill Inserted! </div>');
  //
  //       setTimeout(function() {
  //       location.reload();
  //     }, 1000);
  //   }
  //
  // }
  //   });

         $.redirect("php_action/printSalesBill.php",{
               billNo: billNo,
               billDate: billDate,
               vendorName: vendorName,
               vendorContact: vendorContact,
               totalAmount: totalAmount,
               discount: discount,
               grandTotal: grandTotal,
               products: products,
               sellingPrices: sellingPrices,
               quantities: quantities,
			         productNames: productNames});
  }
  return false;
}
