var manageProductTable;

$(document).ready(function() {
  manageProductTable = $('#manageProductTable').DataTable({
		'ajax': {
            "url": 'php_action/fetchPurchase.php',
            "type": 'post',
            "data":  function( d ){
              d.fromDate= $("#fromDate").val();
              d.toDate= $("#toDate").val();
            },
            dataType: 'json',
        },
        'destroy' : true,
		'order': [],
    'footerCallback': function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return a + parseFloat(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(parseFloat(total).toFixed(2));
        }

  });


  $("#filterBtn").unbind('click').bind('click', function() {
    manageProductTable.ajax.reload();
    return false;
  });
});
