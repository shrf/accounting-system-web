<?php require_once 'includes/header.php'; ?>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">

      <div class="panel-heading">
        <div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Sales Bill</div>
      </div>

      <div class="panel-body">

        <div class="remove-messages"></div>
        <div class="div-action pull pull-right" style="padding-bottom:20px;">
          <form method="get" action="salesBill.php " >
            <button class="btn btn-primary button1"> <i class="glyphicon glyphicon-plus-sign" value="0"></i> Add Bill </button>
          </form>
        </div>

        <label for="fromDate" class="col-sm-1 control-label" style="margin-right:-50px ; margin-top:10px">From</label>
        <div class="col-sm-2">
           <input type="text" class="form-control" name="fromDate"  id="fromDate">
       </div>
         <label for="toDate" class="col-sm-1 control-label" style="margin-right:-70px; margin-left:-25px ; margin-top:10px">To</label>
       <div class="col-sm-2">
          <input type="text" class="form-control" name="toDate"  id="toDate">
      </div>

      <div class="div-action" >
        <button class="btn btn-default" type="button" data-toggle="modal" id="filterBtn" > <i class="glyphicon glyphicon-filter"></i> Filter </button>
      </div>

        <table class="table" id="manageProductTable">
					<thead>
						<tr>
							<th>Bill No.</th>
							<th>Date</th>
							<th>Customer</th>
							<th>Amount</th>
							<th></th>
            </tr>
					</thead>
          <tfoot>
            <tr>
                <th colspan="4" style="text-align:right">Total: </th>
                <th></th>
            </tr>
        </tfoot>
				</table>
    </div>

  </div>
</div>
<script>
$( function() {
  $( "#fromDate" ).datepicker({
    dateFormat: 'dd-mm-yy',
  });
  $("#fromDate").datepicker("setDate", new Date(2017,00,01));
});
$( function() {
  $( "#toDate" ).datepicker({
    dateFormat: 'dd-mm-yy',
  });
  $("#toDate").datepicker("setDate", new Date());
});
</script>

<script src="custom/js/sales.js"></script>

<?php require_once 'includes/footer.php'; ?>
