<?php require_once 'includes/header.php'; ?>
<div class="row">
  <div class="col-md-12">

    <ol class="breadcrumb">
      <li><a href="dashboard.php">Home</a></li>
      <li class="active">Product</li>
    </ol>

    <div class="panel panel-default">

      <div class="panel-heading">
        <div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Manage Product</div>
      </div>

      <div class="panel-body">

        <div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" id="addProductModalBtn" data-target="#addProductModal"> <i class="glyphicon glyphicon-plus-sign"></i> Add Product </button>
        </div>

        <table class="table" id="manageProductTable">
					<thead>
						<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Stocks</th>
							<th>Status</th>
							<th>Purchasing Price</th>
							<th>Selling Price</th>
							<th style="width:15%;">Options</th>
            </tr>
					</thead>
				</table>

      </div>

    </div>

  </div>
</div>

<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	<form class="form-horizontal" id="submitProductForm" action="php_action/createProduct.php" method="POST" enctype="multipart/form-data">

        <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Add Product</h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">
          <div id="add-product-messages"></div>

          <div class="form-group">
	        	<label for="productCode" class="col-sm-3 control-label">Product Code: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="productCode" placeholder="Product Code" name="productCode" autocomplete="off">
				    </div>
	        </div>

	        <div class="form-group">
	        	<label for="productName" class="col-sm-3 control-label">Product Name: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="productName" placeholder="Product Name" name="productName" autocomplete="off">
				    </div>
	        </div>

	        <div class="form-group">
	        	<label for="purchasePrice" class="col-sm-3 control-label">Purchase Price: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="purchasePrice" placeholder="Purchase Price" name="purchasePrice" autocomplete="off">
				    </div>
	        </div>

          <div class="form-group">
	        	<label for="sellingPrice" class="col-sm-3 control-label">Selling Price: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="sellingPrice" placeholder="Selling Price" name="sellingPrice" autocomplete="off">
				    </div>
	        </div>

        </div>

	      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>

	        <button type="submit" class="btn btn-primary" id="createProductBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Save Changes</button>
	      </div>

      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editProductModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Product</h4>
      </div>

      <div class="modal-body" style="max-height:450px; overflow:auto;">

        <div class="div-loading">
      		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
  				<span class="sr-only">Loading...</span>
        </div>

        <div class="div-result">
          <form class="form-horizontal" id="editProductForm" action="php_action/editProduct.php" method="POST">
            <div id="edit-product-messages"></div>

            <div class="form-group">
              <label for="editProductCode" class="col-sm-3 control-label">Product Code: </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="editProductCode" placeholder="Product Code" name="editProductCode" autocomplete="off">
              </div>
            </div>

            <div class="form-group">
              <label for="editProductName" class="col-sm-3 control-label">Product Name: </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="editProductName" placeholder="Product Name" name="editProductName" autocomplete="off">
              </div>
            </div>

            <div class="form-group">
              <label for="editPurchasePrice" class="col-sm-3 control-label">Purchase Price: </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="editPurchasePrice" placeholder="Purchase Price" name="editPurchasePrice" autocomplete="off">
              </div>
            </div>

            <div class="form-group">
              <label for="editSellingPrice" class="col-sm-3 control-label">Selling Price: </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="editSellingPrice" placeholder="Selling Price" name="editSellingPrice" autocomplete="off">
              </div>
            </div>

            <div class="modal-footer editProductFooter">
				        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>
                <button type="submit" class="btn btn-success" id="editProductBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Save Changes</button>
				    </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="removeProductModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Remove Product</h4>
      </div>

      <div class="modal-body">

        <div class="removeProductMessages"></div>
        <p>Do you really want to remove ?</p>
      </div>

      <div class="modal-footer removeProductFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>
        <button type="button" class="btn btn-primary" id="removeProductBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Save changes</button>
      </div>

    </div>
  </div>
</div>

<script src="custom/js/product.js"></script>

<?php require_once 'includes/footer.php'; ?>
