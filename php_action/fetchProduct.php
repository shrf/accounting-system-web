<?php
  require_once 'core.php';

  $sql = "SELECT product_id, product_code, product_name, stock, purchase_price, selling_price
    FROM products WHERE product_status = 0";

  $result = $connect->query($sql);

  $output = array('data' => array());

  if($result->num_rows > 0) {

    $stocks = "";

    while($row = $result->fetch_array()) {

      $productId = $row[0];
      $stock = $row[3];

      if($stock > 0) {
 		     $stocks = "<label class='label label-success'>Available</label>";
 	    } else {
 		     $stocks = "<label class='label label-danger'>Low Stock</label>";
 	    }

 	    $button = '<!-- Single button -->
        <div class="btn-group">
	       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	        Action <span class="caret"></span>
	       </button>
	       <ul class="dropdown-menu">
	        <li><a type="button" data-toggle="modal" id="editProductModalBtn" data-target="#editProductModal" onclick="editProduct('.$productId.')"> <i class="glyphicon glyphicon-edit"></i> Edit</a></li>
	        <li><a type="button" data-toggle="modal" data-target="#removeProductModal" id="removeProductModalBtn" onclick="removeProduct('.$productId.','.$stock.')"> <i class="glyphicon glyphicon-trash"></i> Remove</a></li>
	       </ul>
	      </div>';

	    $output['data'][] = array(
     		$row[1],
     		$row[2],
        $stock,
        $stocks,
        $row[4],
        $row[5],
     		$button
 		 );
   }
}

$connect->close();

echo json_encode($output);

?>
