<?php
  require_once 'core.php';

  $billNo = $_POST['billNo'];

  $sql = "SELECT bill_No, bill_date, customer_name, customer_contact, total_amount, discount, grand_total FROM sales WHERE bill_no = $billNo";
  $result = $connect->query($sql);

  if($result->num_rows > 0) {
    $row = $result->fetch_array();
    $row["date"] = date('d-m-Y', strtotime($row[1]));
    $sql = "SELECT product_name, quantity, rate, total from  sales_items,products  WHERE bill_no = $billNo AND sales_items.product_id = products.product_id ORDER BY sales_item_id";
    $result = $connect->query($sql);
    $products= array();
    $quantity= array();
    $rate= array();
    $total= array();
    while ($array = $result->fetch_array()) {
      array_push($products,$array[0]);
      array_push($quantity,$array[1]);
      array_push($rate,$array[2]);
      array_push($total,$array[3]);
    }
    $row["products"] = $products;
    $row["quantity"] = $quantity;
    $row["rate"] = $rate;
    $row["total"] = $total;
  }

  $connect->close();

  echo json_encode($row);

?>
