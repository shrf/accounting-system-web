<?php
  require_once 'core.php';

  if($_POST) {

    $fromDate= date('Y-m-d',strtotime($_POST['fromDate']));
    $toDate= date('Y-m-d',strtotime($_POST['toDate']));

  $sql = "SELECT bill_no,bill_date, customer_name, total_amount FROM sales WHERE bill_status = 0 and (bill_date between '$fromDate'  and '$toDate' ) order by bill_no desc";

  $result = $connect->query($sql);

  $output = array('data' => array());

  if($result->num_rows > 0) {

    while($row = $result->fetch_array()) {

      $billNo = $row[0];
      $billDate= date('d-m-Y', strtotime($row[1]));

      $button = '<a href="viewSales.php?billNo='.$billNo.'" type="button" > <i class="glyphicon glyphicon-share"></i> View</a>';

	    $output['data'][] = array(
        $billNo,
        $billDate,
        $row[2],
     		$row[3],
        $button
 		   );
    }
 }

$connect->close();

echo json_encode($output);
}
?>
