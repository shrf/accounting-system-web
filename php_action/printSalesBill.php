<?php


  require_once 'core.php';


  if($_POST) {

    $billNo= $_POST['billNo'];
    $billDate= date('Y-m-d',strtotime($_POST['billDate']));
    $vendorName= $_POST['vendorName'];
    $vendorContact= $_POST['vendorContact'];
    $totalAmount= $_POST['totalAmount'];
    $discount= $_POST['discount'];
    $grandTotal= $_POST['grandTotal'];
    $products= $_POST['products'];
$productNames= $_POST['productNames'];
    $sellingPrices= $_POST['sellingPrices'];
    $quantities= $_POST['quantities'];

       $printer = printer_open("Star TSP100 Cutter (TSP143)");
       printer_start_doc($printer, "Doc");
       printer_start_page($printer);
       $barcode = printer_create_font("Arial", 15, 12, PRINTER_FW_NORMAL, false, false, false, 0);
       $arial = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	     $arialb = printer_create_font("Arial", 25, 20, PRINTER_FW_NORMAL, false, false, false, 0);
       printer_select_font($printer, $barcode);

       printer_draw_bmp($printer, "images/logo.bmp", 1, 1);

       printer_draw_text($printer, "Invoice No: ".$billNo, 0, 250);
       printer_draw_text($printer, "Customer: ".$vendorName, 500, 250);
       printer_draw_text($printer, "Invoice No: ".$billNo, 0, 250);
       printer_draw_text($printer, "No.: ".$vendorContact, 500, 350);
       $pen = printer_create_pen(PRINTER_PEN_DOT, 1, "000000");
       printer_select_pen($printer, $pen);
       printer_draw_line($printer, 10, 500, 1860, 500);
	  printer_draw_text($printer, "No", 0, 550);
	  printer_draw_text($printer, "ITEM", 80, 550);
	  printer_draw_text($printer, "PRICE", 300, 550);
	  printer_draw_text($printer, "QTY", 400, 550);
	  printer_draw_text($printer, "AMOUNT", 460, 550);
  	 printer_draw_line($printer, 10, 650, 2000, 650);
    	$i=0;
	for($i=0; $i<count($products); $i++ ){
		printer_draw_text($printer, ($i+1), 0, (700+($i*100)));
	  printer_draw_text($printer, $productNames[$i], 80, (700+($i*100)));
	  printer_draw_text($printer, $sellingPrices[$i], 300, (700+($i*100)));
	  printer_draw_text($printer, $quantities[$i], 410, (700+($i*100)));
	  printer_draw_text($printer, ($sellingPrices[$i]*$quantities[$i]), 490, (700+($i*100)));
       }
	printer_draw_line($printer, 10, (700+($i*100)), 1860, (700+($i*100)));
	printer_select_font($printer, $arial);
	printer_draw_text($printer, "Total      : ".$totalAmount, 1200, (800+($i*100)));
	printer_draw_text($printer, "Discount: ".$discount, 1200, (900+($i*100)));
	printer_draw_line($printer, 10, (900+($i*100)), 1860, (900+($i*100)));
	printer_draw_text($printer, "GRAND TOTAL", 0, (1140+($i*100)));
	printer_select_font($printer, $arialb);
	printer_draw_text($printer, "QR. ".$grandTotal.".00", 300, (1140+($i*100)));
	printer_draw_line($printer, 10, (1250+($i*100)), 1860, (1250+($i*100)));
  printer_select_font($printer, $barcode);
  printer_draw_text($printer, "Thank You Visit Again", 300, (1340+($i*100)));
printer_delete_font($barcode);
       printer_delete_font($arial);
	printer_delete_font($arialb);

    	  printer_delete_pen($pen);

       printer_end_page($printer);

       printer_end_doc($printer);
       printer_close($printer);

	     header("Location: ../salesBill.php");
     exit;

  }


?>
