<?php
require_once '../php_action/db_connect.php';

$sql = "SELECT product_id,product_code,product_name,selling_price from products where product_status = 0;";

$result = mysqli_query($connect,$sql);

$res = array();

while($row = mysqli_fetch_array($result)){
array_push($res, array(
"id"=>$row[0],
"code"=>$row[1],
"name"=>$row[2],
"price"=>$row[3],
));
}

echo json_encode($res);
?>
