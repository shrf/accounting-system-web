<?php
require_once 'php_action/db_connect.php';
require_once 'includes/header.php';

echo "<div class='div-request div-hide'>add</div>";
?>

<ol class="breadcrumb">
  <li><a href="dashboard.php">Home</a></li>
  <li><a href="sales.php">Sales Bill</a></li>
  <li class="active">View Bill </li>
</ol>

<h4>
	<i class='glyphicon glyphicon-circle-arrow-right'></i>
	<?php echo "View Bill"; ?>
</h4>

<div class="panel panel-default">

  <div class="panel-heading">
    <i class="glyphicon glyphicon-info-sign"></i>	View Bill
	</div>

  <div class="panel-body">

    <div class="success-messages"></div>
      <form class="form-horizontal" method="POST" id="createOrderForm">

        <div class="col-md-2">
          <div class="form-group">
           <label for="billNo" class="col-sm-3 control-label">Bill No</label>
           <div class="col-sm-9">
             <input type="text" class="form-control" id="billNo" name="billNo" disabled="true" value="<?php echo $_GET[billNo]; ?>" />
           </div>
          </div>

          <div class="form-group">
          <label for="orderDate" class="col-sm-3 control-label">Bill Date</label>
          <div class="col-sm-9">
             <input type="text" class="form-control" name="orderDate"  id="orderDate" disabled="true">
         </div>
        </div>
      </div>

      <div class="col-md-10">
        <div class="form-group">
         <label for="vendorName" class="col-sm-2 control-label">Customer Name</label>
         <div class="col-sm-10">
           <input type="text" class="form-control" id="vendorName" name="vendorName"  disabled="true" />
         </div>
        </div>

        <div class="form-group">
         <label for="vendorContact" class="col-sm-2 control-label">Customer Contact</label>
         <div class="col-sm-10">
           <input type="text" class="form-control" id="vendorContact" name="vendorContact"  disabled="true" />
         </div>
        </div>
      </div>

      <!-- <div class="div-action pull pull-right" style="padding-bottom:20px;">
        <button type="button" class="btn btn-default button1" onclick="window.location = 'salesBill.php?billNo=<?php echo $_GET[billNo]; ?>'"> <i class="glyphicon glyphicon-plus-sign" value="0"></i> Edit Bill </button>
      </div> -->

        <table class="table" id="manageProductTable">
          <thead>
			  		<tr>
              <th style="width:5%;"></th>
			  			<th style="width:40%;">Product</th>
			  			<th style="width:15%;">Rate</th>
			  			<th style="width:15%;">Quantity</th>
			  			<th style="width:15%;">Total</th>
			  			<th style="width:10%;"></th>
			  		</tr>
			  	</thead>
        </table>

        <div class="col-md-6 pull-right modal-footer">
				  <div class="form-group">
				    <label for="totalAmount" class="col-sm-3 control-label">Total Amount</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="totalAmount"  name="totalAmount" disabled="true"/>
				      <input type="hidden" class="form-control" id="totalAmountValue" name="totalAmountValue" />
				    </div>
				  </div>

          <div class="form-group">
				    <label for="discount" class="col-sm-3 control-label">Discount</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="discount" name="discount"  autocomplete="off" disabled="true"/>
				    </div>
				  </div>

				  <div class="form-group ">
				    <label for="grandTotal" class="col-sm-3 control-label">Grand Total</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="grandTotal" name="grandTotal" disabled="true"   />
				      <input type="hidden" class="form-control" id="grandTotalValue" name="grandTotalValue"/>
				    </div>
				  </div>
        </div>

        <div class="form-group submitButtonFooter ">
			    <div class="col-sm-offset-2 col-sm-10 modal-footer">
            <button type="button" class="btn btn-default" onClick="document.location.href='dashboard.php'"><i class="glyphicon glyphicon-erase"></i> Close</button>
			      <button type="button" id="createOrderBtn" data-loading-text="Loading..." class="btn btn-success" onclick="insert()"><i class="glyphicon glyphicon-ok-sign"></i> Print</button>
			    </div>
			  </div>

      </form>
  </div>
</div>
<script src="custom/js/viewSales.js"></script>

<?php require_once 'includes/footer.php'; ?>
