<?php
require_once 'php_action/db_connect.php';
require_once 'includes/header.php';

echo "<div class='div-request div-hide'>add</div>";
// } else if($_GET['o'] == 'manord') {
// 	echo "<div class='div-request div-hide'>manord</div>";
// } else if($_GET['o'] == 'editOrd') {
// 	echo "<div class='div-request div-hide'>editOrd</div>";
// }

?>

<ol class="breadcrumb">
  <li><a href="dashboard.php">Home</a></li>
  <li><a href="purchase.php">Purchase Bill</a></li>
  <li class="active">
  	<?
    // php if($_GET['o'] == 'add') {
      ?>
  		Add Bill
		<?php
  // } else if($_GET['o'] == 'manord') {
  ?>
			<!-- Manage Order -->
		<?php
  // }
   ?>
  </li>
</ol>

<h4>
	<i class='glyphicon glyphicon-circle-arrow-right'></i>
	<?php
  // if($_GET['o'] == 'add') {
		echo "Add Order";
	// } else if($_GET['o'] == 'manord') {
		// echo "Manage Order";
	// } else if($_GET['o'] == 'editOrd') {
		// echo "Edit Order";
	// }
	?>
</h4>

<div class="panel panel-default">

  <div class="panel-heading">
    <?php
    //  if($_GET['o'] == 'add') {
    ?>
  		<i class="glyphicon glyphicon-plus-sign"></i>	Add Order
		<?php
  // } else if($_GET['o'] == 'manord') {
   ?>
			<!-- <i class="glyphicon glyphicon-edit"></i> Manage Order -->
		<?php
  //  } else if($_GET['o'] == 'editOrd') {
   ?>
			<!-- <i class="glyphicon glyphicon-edit"></i> Edit Order -->
		<?php
  // }
  //  ?>
  </div>

  <div class="panel-body">

    <?php
    //  if($_GET['o'] == 'add') {
    ?>
      <div class="success-messages"></div>
      <form class="form-horizontal" method="POST" id="createOrderForm">

        <div class="col-md-2">
          <div class="form-group">
           <label for="billNo" class="col-sm-3 control-label">Bill No</label>
           <div class="col-sm-9">
             <?php
               $billSql = "SELECT COALESCE(MAX(bill_no),999)+1 FROM purchase";
               $billData = $connect->query($billSql);
               if($row = $billData->fetch_array())
                 echo '<input type="text" class="form-control" id="billNo" name="billNo" value="'.$row[0].'" disabled="true" />';
             ?>

           </div>
          </div>

          <div class="form-group">
          <label for="orderDate" class="col-sm-3 control-label">Bill Date</label>
          <div class="col-sm-9">
             <input type="text" class="form-control" name="orderDate"  id="orderDate">
         </div>
        </div>
      </div>

      <div class="col-md-10">
        <div class="form-group">
         <label for="vendorName" class="col-sm-2 control-label">Vendor Name</label>
         <div class="col-sm-10">
           <input type="text" class="form-control" id="vendorName" name="vendorName" placeholder="Vendor Name" autocomplete="off" />
         </div>
        </div>

        <div class="form-group">
         <label for="vendorContact" class="col-sm-2 control-label">Vendor Contact</label>
         <div class="col-sm-10">
           <input type="text" class="form-control" id="vendorContact" name="vendorContact" placeholder="Contact Number" autocomplete="off" />
         </div>
        </div>
      </div>

        <div class="div-action pull pull-right" style="padding-top:20px;">
					<button class="btn btn-primary" type="button" data-toggle="modal" id="addProductModalBtn" data-target="#addProductModal"> <i class="glyphicon glyphicon-plus-sign"></i> Add to Bill </button>
        </div>

        <table class="table" id="manageProductTable">
          <thead>
			  		<tr>
              <th style="width:5%;"></th>
			  			<th style="width:40%;">Product</th>
			  			<th style="width:15%;">Rate</th>
			  			<th style="width:15%;">Quantity</th>
			  			<th style="width:15%;">Total</th>
			  			<th style="width:10%;"></th>
			  		</tr>
			  	</thead>
        </table>

        <div class="col-md-6 pull-right modal-footer">
				  <div class="form-group">
				    <label for="totalAmount" class="col-sm-3 control-label">Total Amount</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="totalAmount" value="0" name="totalAmount" disabled="true"/>
				      <input type="hidden" class="form-control" id="totalAmountValue" name="totalAmountValue" />
				    </div>
				  </div>

          <div class="form-group">
				    <label for="discount" class="col-sm-3 control-label">Discount</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="discount" name="discount" onkeyup="discountFunc()" autocomplete="off" value="0"/>
				    </div>
				  </div>

				  <div class="form-group ">
				    <label for="grandTotal" class="col-sm-3 control-label">Grand Total</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="grandTotal" name="grandTotal" disabled="true"   value="0"/>
				      <input type="hidden" class="form-control" id="grandTotalValue" name="grandTotalValue"/>
				    </div>
				  </div>
        </div>

        <div class="form-group submitButtonFooter ">
			    <div class="col-sm-offset-2 col-sm-10 modal-footer">
            <button type="button" class="btn btn-default" onclick="window.location.reload()"><i class="glyphicon glyphicon-erase"></i> Reset</button>
			      <button type="button" id="createOrderBtn" data-loading-text="Loading..." class="btn btn-success" onclick="insert()"><i class="glyphicon glyphicon-ok-sign"></i> Save Changes</button>
			    </div>
			  </div>
      </form>
    <?php
  //  }
   ?>
  </div>
</div>

<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	<form class="form-horizontal" id="submitProductForm"  method="POST" enctype="multipart/form-data">

        <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Add to Bill</h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">
          <div id="add-product-messages"></div>

          <div class="form-group">
	        	<label for="productCode" class="col-sm-3 control-label">Product Code: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="productCode" placeholder="Product Code" name="productCode" autocomplete="off">
				    </div>
	        </div>

	        <div class="form-group">
            <label for="productCode" class="col-sm-3 control-label">Product Name: </label>
            <div class="col-sm-8">
              <select class="form-control" name="productName" id="productName" onchange="getProductData" >
    						<option value="">~~SELECT~~</option>
    						<?php
    							$productSql = "SELECT * FROM products";
    							$productData = $connect->query($productSql);

    							while($row = $productData->fetch_array()) {
    								echo "<option value='".$row['product_id']."' id='changeProduct".$row['product_id']."'>".$row['product_name']."</option>";
    						 	}
    						?>
  						</select>
            </div>
	        </div>

          <div class="col-md-6">
            <div class="form-group">
            	<label for="purchasePrice" class="col-sm-3 control-label">Purchase Price: </label>
            	<div class="col-sm-8">
    			      <input type="text" class="form-control" id="purchasePrice" placeholder="Purchase Price" name="purchasePrice" autocomplete="off">
    			    </div>
            </div>

            <div class="form-group">
  	        	<label for="stocks" class="col-sm-3 control-label">Current Stock: </label>
  	        	<div class="col-sm-8">
  				      <input type="text" class="form-control" id="stocks" placeholder="Current Stocks" name="stocks" disabled="true">
  				    </div>
  	        </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
            	<label for="sellingPrice" class="col-sm-3 control-label">Selling Price: </label>
            	<div class="col-sm-8">
    			      <input type="text" class="form-control" id="sellingPrice" placeholder="Selling Price" name="sellingPrice" autocomplete="off">
    			    </div>
            </div>

            <div class="form-group">
  	        	<label for="quantity" class="col-sm-3 control-label">Quantity: </label>
  	        	<div class="col-sm-8">
  				      <input type="text" min="1"default class="form-control" id="quantity" placeholder="Quantity" name="quantity" autocomplete="off">
  				    </div>
  	        </div>
          </div>
        </div>

	      <div class="modal-footer">
          <input type="hidden" name="productId" id="productId" value=""/>
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>

	        <button type="submit" class="btn btn-primary" id="createProductBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Add to Bill</button>
	      </div>
``
      </form>
    </div>
  </div>
</div>



<script>
$( function() {
  $( "#orderDate" ).datepicker({
    dateFormat: 'dd-mm-yy',
  });
  $("#orderDate").datepicker("setDate", new Date(2017,01,01));
});
</script>
<script src="custom/js/purchaseBill.js"></script>

<?php require_once 'includes/footer.php'; ?>
